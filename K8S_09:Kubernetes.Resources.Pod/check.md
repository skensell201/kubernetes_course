$ k create -f first-pod.yaml
pod/first-pod created

$ k describe pod first-pod
Name:         first-pod
Namespace:    default
Priority:     0
Node:         gke-my-first-cluster-1-default-pool-58295828-n6pb/10.128.0.48
Start Time:   Thu, 11 Jun 2020 14:02:21 +0300
Labels:       app=skense1_at_yandex_ru
Annotations:  kubectl.kubernetes.io/last-applied-configuration:
                {"apiVersion":"v1","kind":"Pod","metadata":{"annotations":{},"labels":{"app":"skense1_at_yandex_ru"},"name":"first-pod","namespace":"defau...
              kubernetes.io/limit-ranger: LimitRanger plugin set: cpu request for container frontend
Status:       Running
IP:           10.40.1.4
IPs:
  IP:  10.40.1.4
Containers:
  frontend:
    Container ID:   docker://48d2e3043bca396cf7341652d52249e42409aa4bb9061dd8ff5f5851bd9c1d37
    Image:          godlovedc/lolcow
    Image ID:       docker-pullable://godlovedc/lolcow@sha256:a692b57abc43035b197b10390ea2c12855d21649f2ea2cc28094d18b93360eeb
    Port:           <none>
    Host Port:      <none>
    State:          Waiting
      Reason:       CrashLoopBackOff
    Last State:     Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 11 Jun 2020 14:16:05 +0300
      Finished:     Thu, 11 Jun 2020 14:16:05 +0300
    Ready:          False
    Restart Count:  6
    Requests:
      cpu:        100m
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-4n8db (ro)
Conditions:
  Type              Status
  Initialized       True
  Ready             False
  ContainersReady   False
  PodScheduled      True
Volumes:
  default-token-4n8db:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-4n8db
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type     Reason     Age                    From                                                        Message
  ----     ------     ----                   ----                                                        -------
  Normal   Scheduled  14m                    default-scheduler                                           Successfully assigned default/first-pod to gke-my-first-cluster-1-default-pool-58295828-n6pb
  Normal   Pulling    4m15s (x3 over 4m57s)  kubelet, gke-my-first-cluster-1-default-pool-58295828-n6pb  Pulling image "odlovedc/lolcow:latest"
	
$ k logs first-pod
 ______________________________________
/ Q: How much does it cost to ride the \
\ Unibus? A: 2 bits.                   /
 --------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
								
$ cat first-pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: first-pod
  labels:
    app: skense1_at_yandex_ru
spec:
  containers:
  - name: frontend
    image: godlovedc/lolcow