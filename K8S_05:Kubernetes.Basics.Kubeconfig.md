```
$ kubectl config get-contexts
CURRENT   NAME             CLUSTER     AUTHINFO    NAMESPACE
  *       first-cluster    minikube    minikube
        second-cluster   minikube1   minikube1
					

```
```
$ kubectl cluster-info --context="first-cluster"
Kubernetes master is running at https://142.93.50.102:8443
KubeDNS is running at https://142.93.50.102:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

```
```
$ kubectl cluster-info --context="second-cluster"
Kubernetes master is running at https://142.93.54.10:8443
KubeDNS is running at https://142.93.54.10:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

```
```
$ cat .kube/config
apiVersion: v1
clusters:
- cluster:
    certificate-authority: /Users/ivan_kostin/.kube/first-cluster/ca.crt
    server: https://142.93.50.102:8443
  name: minikube
- cluster:
    certificate-authority: /Users/ivan_kostin/.kube/second-cluster/ca.crt
    server: https://142.93.54.10:8443
  name: minikube1
contexts:
- context:
    cluster: minikube
    user: minikube
  name: first-cluster
- context:
    cluster: minikube1
    user: minikube1
  name: second-cluster
current-context: first-cluster
kind: Config
preferences: {}
users:
- name: minikube
  user:
    client-certificate: /Users/ivan_kostin/.kube/first-cluster/client.crt
    client-key: /Users/ivan_kostin/.kube/first-cluster/client.key
- name: minikube1
  user:
    client-certificate: /Users/ivan_kostin/.kube/second-cluster/client.crt
    client-key: /Users/ivan_kostin/.kube/second-cluster/client.key
```