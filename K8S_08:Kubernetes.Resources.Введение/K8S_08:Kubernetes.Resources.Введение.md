### 1-й вариант (с телом запроса)
```
curl -X POST \
--url https://34.72.44.13:443/apis/apps/v1/namespaces/default/deployments \
-H "Authorization: Bearer $TOKEN" --insecure \
-H 'content-type: application/json' \
 -d '{"apiVersion":"apps/v1","kind":"Deployment","metadata":{"name":"nginx-deployment","labels":{"app":"nginx"}},"spec":{"replicas":3,"selector":{"matchLabels":{"app":"nginx"}},"template":{"metadata":{"labels":{"app":"nginx"}},"spec":{"containers":[{"name":"nginx","image":"nginx:1.7.9","ports":[{"containerPort":80}]}]}}}}'
 
{
  "kind": "Deployment",
  "apiVersion": "apps/v1",
  "metadata": {
    "name": "nginx-deployment",
    "namespace": "default",
    "selfLink": "/apis/apps/v1/namespaces/default/deployments/nginx-deployment",
    "uid": "8284b24d-89d6-4076-b269-6b456f5fbe25",
    "resourceVersion": "14525",
    "generation": 1,
    "creationTimestamp": "2020-06-05T12:07:04Z",
    "labels": {
      "app": "nginx"
    }
  },
  "spec": {
    "replicas": 3,
    "selector": {
      "matchLabels": {
        "app": "nginx"
      }
    },
    "template": {
      "metadata": {
        "creationTimestamp": null,
        "labels": {
          "app": "nginx"
        }
      },
      "spec": {
        "containers": [
          {
            "name": "nginx",
            "image": "nginx:1.7.9",
            "ports": [
              {
                "containerPort": 80,
                "protocol": "TCP"
              }
            ],
            "resources": {

            },
            "terminationMessagePath": "/dev/termination-log",
            "terminationMessagePolicy": "File",
            "imagePullPolicy": "IfNotPresent"
          }
        ],
        "restartPolicy": "Always",
        "terminationGracePeriodSeconds": 30,
        "dnsPolicy": "ClusterFirst",
        "securityContext": {

        },
        "schedulerName": "default-scheduler"
      }
    },
    "strategy": {
      "type": "RollingUpdate",
      "rollingUpdate": {
        "maxUnavailable": "25%",
        "maxSurge": "25%"
      }
    },
    "revisionHistoryLimit": 10,
    "progressDeadlineSeconds": 600
  },
  "status": {

  }
}%


```

### 2-й вариант (без тела запроса)
```
curl -X POST \
--url https://34.72.44.13:443/apis/apps/v1/namespaces/default/deployments \
-H  "Authorization: Bearer $TOKEN" --insecure \
-H  'content-type: application/json' \
 -d @k8s.json
```