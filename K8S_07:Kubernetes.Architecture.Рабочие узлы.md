```
$ k run nginx-demo --image ubuntu:latest --labels="name=nginx-demo" --port=80 --command /usr/bin/tail -- -f /dev/null

```
```
$ k apply -f nginx-demo-nodeport-svc.yml
service/nginx-demo unchanged

```
```
$ k get svc
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)                      AGE
kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP                      3h57m
nginx-demo   NodePort    10.97.76.188   <none>        80:30180/TCP,443:31443/TCP   3h4m

```
```
$ journalctl -tail -n20 -u kubelet
-- Logs begin at Thu 2020-06-04 17:47:22 UTC, end at Thu 2020-06-04 17:58:50 UTC. --
-- No entries --

,
    "restartPolicy": "Always",
    "terminationGracePeriodSeconds": 30,
    "dnsPolicy": "ClusterFirst",
    "nodeSelector": {
      "kubernetes.io/os": "linux"
    },
    "serviceAccountName": "kube-proxy",
    "serviceAccount": "kube-proxy",
    "nodeName": "minikube",
    "hostNetwork": true,
    "securityContext": {

    },
```