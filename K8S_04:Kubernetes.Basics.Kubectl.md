```
$ kubectl run k8s-task03 --image ubuntu:bionic --command /usr/bin/tail -- -f /dev/null
kubectl run --generator=deployment/apps.v1 
Is DEPRECATED and will be removed in a future version. 
Use kubectl run --generator=run-pod/v1 or kubectl create instead.
deployment.apps/k8s-task03 created
```
```
$ kubectl get pods
NAME                          READY   STATUS    RESTARTS   AGE
k8s-task03-674cc44746-r688k   1/1     Running   0          3s
```
```
$ kubectl exec -it k8s-task03-674cc44746-r688k uptime
 08:56:30 up 19:27,  0 users,  load average: 0.08, 0.26, 0.22
```
```
$ kubectl get pod k8s-task03-674cc44746-r688k -o=custom-columns=NODE:.spec.nodeName,NAME:.metadata.name
NODE                                        NAME
gke-k8s-course-default-pool-a854ae0f-gnc5   k8s-task03-674cc44746-r688k
```