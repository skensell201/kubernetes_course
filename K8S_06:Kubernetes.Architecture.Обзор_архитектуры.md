```
$ curl http://localhost:8080/api/v1/namespaces/kube-system/pods/kube-apiserver-docker-desktop | head -n 14

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  6696    0  6696    0     0  1634k      0 --:--:-- --:--:-- --:--:-- 1634k
{
  "kind": "Pod",
  "apiVersion": "v1",
  "metadata": {
    "name": "kube-apiserver-docker-desktop",
    "namespace": "kube-system",
    "selfLink": "/api/v1/namespaces/kube-system/pods/kube-apiserver-docker-desktop",
    "uid": "204c4b9d-f212-4160-ae76-7e68205d6f75",
    "resourceVersion": "488",
    "creationTimestamp": "2020-06-04T11:20:39Z",
    "labels": {
      "component": "kube-apiserver",
      "tier": "control-plane"
    },

```    
```
$ curl http://localhost:8080/api/v1/namespaces/kube-system/pods/kube-controller-manager-docker-desktop | head -n 14

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  6335    0  6335    0     0  1546k      0 --:--:-- --:--:-- --:--:-- 1546k
{
  "kind": "Pod",
  "apiVersion": "v1",
  "metadata": {
    "name": "kube-controller-manager-docker-desktop",
    "namespace": "kube-system",
    "selfLink": "/api/v1/namespaces/kube-system/pods/kube-controller-manager-docker-desktop",
    "uid": "acd4731a-af00-4c15-b2fe-f39864599482",
    "resourceVersion": "489",
    "creationTimestamp": "2020-06-04T11:20:47Z",
    "labels": {
      "component": "kube-controller-manager",
      "tier": "control-plane"
    },

```    
```
$ curl http://localhost:8080/api/v1/namespaces/kube-system/pods/kube-scheduler-docker-desktop | head -n 14

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  4140    0  4140    0     0  1347k      0 --:--:-- --:--:-- --:--:-- 1347k
{
  "kind": "Pod",
  "apiVersion": "v1",
  "metadata": {
    "name": "kube-scheduler-docker-desktop",
    "namespace": "kube-system",
    "selfLink": "/api/v1/namespaces/kube-system/pods/kube-scheduler-docker-desktop",
    "uid": "607edb71-b388-4ae3-9a3a-da66f555b7e2",
    "resourceVersion": "586",
    "creationTimestamp": "2020-06-04T11:20:55Z",
    "labels": {
      "component": "kube-scheduler",
      "tier": "control-plane"
    },   

``` 
```
$ kubectl --namespace kube-system logs kube-apiserver-docker-desktop | head -n 3
Flag --insecure-port has been deprecated, This flag will be removed in a future version.
I0604 11:19:30.534778       1 server.go:623] external host was not specified, using 192.168.65.3
I0604 11:19:30.535534       1 server.go:149] Version: v1.16.6-beta.0   

```
```
$ kubectl --namespace kube-system logs kube-controller-manager-docker-desktop | head -n 3
I0604 11:19:31.057334       1 serving.go:319] Generated self-signed cert in-memory
I0604 11:19:31.439035       1 controllermanager.go:161] Version: v1.16.6-beta.0
I0604 11:19:31.439481       1 secure_serving.go:123] Serving securely on 127.0.0.1:10257  


```
```
$ kubectl --namespace kube-system logs kube-scheduler-docker-desktop | head -n 3
I0604 11:19:30.940072       1 serving.go:319] Generated self-signed cert in-memory
W0604 11:19:33.443073       1 authentication.go:262] Unable to get configmap/extension-apiserver-authentication in kube-system.  Usually fixed by 'kubectl create rolebinding -n kube-system ROLEBINDING_NAME --role=extension-apiserver-authentication-reader --serviceaccount=YOUR_NS:YOUR_SA'
W0604 11:19:33.443092       1 authentication.go:199] Error looking up in-cluster authentication configuration: configmaps "extension-apiserver-authentication" is forbidden: User "system:kube-scheduler" cannot get resource "configmaps" in API group "" in the namespace "kube-system"

```
```
$ minikube start --extra-config=controller-manager.enable-garbage-collector=true
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube"

```
```
$ etcd --listen-client-urls=http://localhost:2379 --advertise-client-urls=http://localhost:2379

$ etcdctl put key value
OK

$ etcdctl get key
key
value
```