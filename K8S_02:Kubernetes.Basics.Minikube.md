```
$ k config view --output='json' | jq '[.clusters[].name, .clusters[].cluster.server]'
[
  "minikube",
  "https://192.168.99.104:8443"
]

$ curl https://192.168.99.104:8443
curl: (60) SSL certificate problem: unable to get local issuer certificate
More details here: https://curl.haxx.se/docs/sslcerts.html

curl performs SSL certificate verification by default, using a "bundle"
 of Certificate Authority (CA) public keys (CA certs). If the default
 bundle file isn't adequate, you can specify an alternate file
 using the --cacert option.
If this HTTPS server uses a certificate signed by a CA represented in
 the bundle, the certificate verification probably failed due to a
 problem with the certificate (it might be expired, or the name might
 not match the domain name in the URL).
If you'd like to turn off curl's verification of the certificate, use
 the -k (or --insecure) option.
HTTPS-proxy has similar options --proxy-cacert and --proxy-insecure.

$ curl -v -k -H --cacert ~/.minikube/ca.crt -H "Authorization: Bearer $(cat ~/.minikube/token)"  "https://192.168.99.104:8443/api"
>
* Connection state changed (MAX_CONCURRENT_STREAMS updated)!
< HTTP/2 200
< content-type: application/json
< content-length: 186
< date: Wed, 11 Dec 2019 09:26:27 GMT
<
{
  "kind": "APIVersions",
  "versions": [
    "v1"
  ],
  "serverAddressByClientCIDRs": [
    {
      "clientCIDR": "0.0.0.0/0",
      "serverAddress": "192.168.99.104:8443"
    }
  ]
* Connection #0 to host 192.168.99.104 left intact
}%

# С помощью этой команды получаем имя сервисного-аккаунта и передаем его в переменную окружения
export secret=$(kubectl get serviceaccount default -o jsonpath='{.secrets[0].name}')

# С помощью этой команды мы генерируем токен для сервисного аккаунта

$ k get secret $secret -o yaml | grep "token:" | awk {'print $2'} |  base64 -D > ~/.minikube/token
```